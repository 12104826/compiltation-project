# Function to read and return the contents of a file
def read_file(filename):
    try:
        with open(filename, 'r') as file:
            return file.read()
    except FileNotFoundError:
        print(f"File {filename} not found.")
        return ""

# Function to check if an expression is constant
def is_constant_expression(node):
    if isinstance(node, tuple):
        if node[0] in ('num', 'bool'):
            return True
        elif node[0] in ('+', '-', '*', '%', '==', '>', '>=', 'uminus', '!'):
            return is_constant_expression(node[1]) and is_constant_expression(node[2])
        elif node[0] == '&&':
            return is_constant_expression(node[1]) and is_constant_expression(node[2])
    return False

# Function to evaluate a constant expression
def evaluate_constant_expression(node):
    if node[0] == 'num':
        return node[1]
    elif node[0] == 'bool':
        return node[1]
    elif node[0] == '+':
        return evaluate_constant_expression(node[1]) + evaluate_constant_expression(node[2])
    elif node[0] == '-':
        return evaluate_constant_expression(node[1]) - evaluate_constant_expression(node[2])
    elif node[0] == '*':
        return evaluate_constant_expression(node[1]) * evaluate_constant_expression(node[2])
    elif node[0] == '%':
        return evaluate_constant_expression(node[1]) % evaluate_constant_expression(node[2])
    elif node[0] == '==':
        return evaluate_constant_expression(node[1]) == evaluate_constant_expression(node[2])
    elif node[0] == '>':
        return evaluate_constant_expression(node[1]) > evaluate_constant_expression(node[2])
    elif node[0] == '>=':
        return evaluate_constant_expression(node[1]) >= evaluate_constant_expression(node[2])
    elif node[0] == 'uminus':
        return -evaluate_constant_expression(node[1])
    elif node[0] == '!':
        return not evaluate_constant_expression(node[1])
    elif node[0] == '&&':
        return evaluate_constant_expression(node[1]) and evaluate_constant_expression(node[2])
    return None

# Function to compile AST into instructions
def compile_ast(commands):
    assembly_code = []
    label_count = 0
    functions = {}

    def generate_code(node):
        nonlocal label_count

        # Check if the expression is a constant expression
        if is_constant_expression(node):
            result = evaluate_constant_expression(node)
            if isinstance(result, bool):
                assembly_code.append(f'CsteBo {str(result).lower()}')
            elif isinstance(result, int):
                assembly_code.append(f'CsteNb {result}')
            else:
                assembly_code.append(f'CsteNb {result}')
            return

        if node[0] == 'num':
            assembly_code.append(f'CsteNb {node[1]}')
        elif node[0] == 'bool':
            assembly_code.append(f'CsteBo {str(node[1]).lower()}')
        elif node[0] == 'var':
            assembly_code.append(f'GetVar {node[1]}')
        elif node[0] == 'undefined':
            assembly_code.append('CsteUn')
        elif node[0] in ('+', '-', '*', '%', '==', '>', '>='):
            generate_code(node[1])
            # Implicit cast to number if the type is boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('NbToBo')
            generate_code(node[2])
            # Implicit cast to number if the type is boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('NbToBo')
            if node[0] == '+':
                assembly_code.append('AddiNb')
            elif node[0] == '-':
                assembly_code.append('SubsNb')
            elif node[0] == '*':
                assembly_code.append('MultNb')
            elif node[0] == '%':
                assembly_code.append('ModuNb')
            elif node[0] == '==':
                assembly_code.append('Equals')
            elif node[0] == '>':
                assembly_code.append('GrStNb')
            elif node[0] == '>=':
                assembly_code.append('GrEqNb')
        elif node[0] == '&&':
            generate_code(node[1])
            # Implicit cast to boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('BoToNb')
            label_count += 1
            false_label = f'FALSE_{label_count}'
            end_label = f'END_{label_count}'
            assembly_code.append(f'ConJmp {false_label}')
            generate_code(node[2])
            # Implicit cast to boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('BoToNb')
            assembly_code.append(f'Jump {end_label}')
            assembly_code.append(f'{false_label}:')
            assembly_code.append('CsteBo false')
            assembly_code.append(f'{end_label}:')
        elif node[0] == 'uminus':
            generate_code(node[1])
            assembly_code.append('NegaNb')
        elif node[0] == '!':
            generate_code(node[1])
            # Implicit cast to boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('BoToNb')
            assembly_code.append('Not')
        elif node[0] == 'assign':
            generate_code(node[2])
            assembly_code.append(f'SetVar {node[1]}')
        elif node[0] == 'import':
            filename = f"{node[1]}.jsm"
            file_content = read_file(filename)
            assembly_code.append(file_content)
        elif node[0] == 'if':
            generate_code(node[1])
            # Implicit cast to boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('BoToNb')
            label_count += 1
            else_label = f'ELSE_{label_count}'
            end_label = f'END_{label_count}'
            assembly_code.append(f'ConJmp {else_label}')
            generate_code(node[2])
            assembly_code.append(f'Jump {end_label}')
            assembly_code.append(f'{else_label}:')
            generate_code(node[3])
            assembly_code.append(f'{end_label}:')
        elif node[0] == 'do_while':
            label_count += 1
            start_label = f'START_{label_count}'
            end_label = f'END_{label_count}'
            assembly_code.append(f'{start_label}:')
            generate_code(node[1])
            generate_code(node[2])
            # Implicit cast to boolean
            assembly_code.append('TypeOf')
            assembly_code.append('Case')
            assembly_code.append('BoToNb')
            assembly_code.append(f'ConJmp {end_label}')
            assembly_code.append(f'Jump {start_label}')
            assembly_code.append(f'{end_label}:')
        elif node[0] == 'block':
            for cmd in node[1]:
                generate_code(cmd)
        elif node[0] == 'empty':
            pass
        elif node[0] == 'call':
            for i, arg in enumerate(node[2]):
                generate_code(arg)
                assembly_code.append(f'SetArg {i}')
            assembly_code.append(f'Call {node[1]}')
        elif node[0] == 'function':
            function_label = f'FUNC_{node[1]}'
            functions[node[1]] = function_label
            assembly_code.append(f'{function_label}:')
            assembly_code.append('NewClot')
            for i, param in enumerate(node[2]):
                assembly_code.append(f'DclArg {param} {i}')
            for cmd in node[3]:
                generate_code(cmd)
            assembly_code.append('Return')
        elif node[0] == 'return':
            generate_code(node[1])
            assembly_code.append('Return')

    for command in commands:
        generate_code(command)
    assembly_code.append('Halt')

    for line in assembly_code:
        print(line)
