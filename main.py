from parser import parse
from code_gen import compile_ast

def test_expression(expression):
    print(f"Testing expression: {expression}")
    ast = parse(expression)
    if ast:
        compile_ast(ast)
    else:
        print("Parsing failed.")

if __name__ == '__main__':
    test_expression("2+3*5;")

# if __name__ == '__main__':
#     while True:
#         try:
#             s = input('input > ')
#         except EOFError:
#             break
#         if not s:
#             continue

#         ast = parse(s)
#         if ast:
#             compile_ast(ast)


