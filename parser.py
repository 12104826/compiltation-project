import ply.yacc as yacc
from lexer import tokens

# Precedence rules for the parser
precedence = (
    ('right', 'ASSIGN'),
    ('right', 'UMINUS'),
    ('right', 'NOT'),
    ('left', 'GT', 'GE', 'EQ'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'MOD'),
    ('left', 'AND')
)

# Parsing rules

def p_program(p):
    '''program : command_list'''
    p[0] = p[1]

def p_command_list(p):
    '''command_list : command_list command
                    | command'''
    if len(p) == 3:
        p[0] = p[1] + [p[2]]
    else:
        p[0] = [p[1]]

def p_command_import(p):
    '''command : IMPORT IDENTIFIER SEMICOLON'''
    p[0] = ('import', p[2])

def p_command_expression(p):
    '''command : expression SEMICOLON'''
    p[0] = p[1]

def p_command_assignment(p):
    '''command : IDENTIFIER ASSIGN expression SEMICOLON'''
    p[0] = ('assign', p[1], p[3])

def p_command_if(p):
    '''command : IF LPAREN expression RPAREN command ELSE command'''
    p[0] = ('if', p[3], p[5], p[7])

def p_command_do_while(p):
    '''command : DO command WHILE LPAREN expression RPAREN SEMICOLON'''
    p[0] = ('do_while', p[2], p[5])

def p_command_empty(p):
    '''command : SEMICOLON'''
    p[0] = ('empty',)

def p_command_block(p):
    '''command : LBRACE command_list RBRACE'''
    p[0] = ('block', p[2])

def p_command_call(p):
    '''command : CALL IDENTIFIER LPAREN arg_list RPAREN SEMICOLON'''
    p[0] = ('call', p[2], p[4])

def p_arg_list(p):
    '''arg_list : arg_list COMMA expression
                | expression
                | empty'''
    if len(p) == 4:
        p[0] = p[1] + [p[3]]
    elif len(p) == 2:
        if p[1] is None:
            p[0] = []
        else:
            p[0] = [p[1]]

def p_empty(p):
    '''empty : '''
    p[0] = None

def p_expression_comment(p):
    '''expression : COMMENT'''
    pass  # Ignore comments

def p_expression_binop(p):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression MOD expression
                  | expression EQ expression
                  | expression GT expression
                  | expression GE expression
                  | expression AND expression'''
    p[0] = (p[2], p[1], p[3])

def p_expression_uminus(p):
    'expression : MINUS expression %prec UMINUS'
    p[0] = ('uminus', p[2])

def p_expression_not(p):
    'expression : NOT expression'
    p[0] = ('!', p[2])

def p_expression_group(p):
    'expression : LPAREN expression RPAREN'
    p[0] = p[2]

def p_expression_boolean(p):
    '''expression : TRUE
                  | FALSE'''
    p[0] = ('bool', p[1])

def p_expression_number(p):
    '''expression : NUMBER
                  | NAN'''
    p[0] = ('num', p[1])

def p_expression_identifier(p):
    '''expression : IDENTIFIER'''
    p[0] = ('var', p[1])

def p_expression_undefined(p):
    '''expression : UNDEFINED'''
    p[0] = ('undefined',)

def p_command_function_declaration(p):
    '''command : FUNCTION IDENTIFIER LPAREN param_list RPAREN LBRACE command_list RBRACE'''
    p[0] = ('function', p[2], p[4], p[7])

def p_param_list(p):
    '''param_list : param_list COMMA IDENTIFIER
                  | IDENTIFIER
                  | empty'''
    if len(p) == 4:
        p[0] = p[1] + [p[3]]
    elif len(p) == 2:
        if p[1] is None:
            p[0] = []
        else:
            p[0] = [p[1]]

def p_command_return(p):
    '''command : RETURN expression SEMICOLON'''
    p[0] = ('return', p[2])

def p_expression_function(p):
    '''expression : FUNCTION LPAREN param_list RPAREN LBRACE command_list RBRACE'''
    p[0] = ('closure', p[3], p[6])
    
def p_error(p):
    print("Syntax error at '%s'" % p.value if p else "Syntax error at EOF")

# Build the parser
parser = yacc.yacc()

# Function to parse input data
def parse(data):
    return parser.parse(data)
