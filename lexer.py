import ply.lex as lex


# Token definitions
tokens = (
    'NUMBER', 'PLUS', 'MINUS', 'TIMES', 'MOD',
    'LPAREN', 'RPAREN', 'LBRACE', 'RBRACE', 'SEMICOLON', 
    'TRUE', 'FALSE', 'EQ', 'NOT', 'GT', 'GE', 'NAN', 
    'IMPORT', 'IDENTIFIER', 'AND', 'COMMENT', 'ASSIGN', 
    'IF', 'ELSE', 'DO', 'WHILE', 'CALL'
)

# Regular expression rules for simple tokens
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_MOD = r'%'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_SEMICOLON = r';'
t_EQ = r'=='
t_NOT = r'!'
t_GT = r'>'
t_GE = r'>='
t_AND = r'&&'
t_ASSIGN = r'='

# A string containing ignored characters (spaces and tabs)
t_ignore = ' \t'

# Keywords in lower-case
reserved = {
    'import': 'IMPORT',
    'if': 'IF',
    'else': 'ELSE',
    'do': 'DO',
    'while': 'WHILE',
    'true': 'TRUE',
    'false': 'FALSE',
    'nan': 'NAN',
    'call': 'CALL'
}

# Define a rule for identifiers (used for variable names and function names)
def t_IDENTIFIER(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value.lower(), 'IDENTIFIER')    # Check for reserved words
    return t

# Define a rule for number token (both integer and floating point)
def t_NUMBER(t):
    r'\d+(\.\d+)?([eE][+-]?\d+)?'
    try:
        t.value = float(t.value) if '.' in t.value or 'e' in t.value or 'E' in t.value else int(t.value)
    except ValueError:
        print(f"Invalid number {t.value}")
        t.value = 0
    return t

# Define rules for comments
def t_COMMENT(t):
    r'(/\*(.|\n)*?\*/)|(//.*)'
    pass  # Token is ignored

# Define a rule for handling errors
def t_error(t):
    print(f"Illegal character '{t.value[0]}'")
    t.lexer.skip(1)

def t_FUNCTION(t):
    r'function'
    return t

def t_RETURN(t):
    r'return'
    return t


def t_UNDEFINED(t):
    r'undefined'
    t.value = None  # Use None to represent the undefined value in the AST
    return t

# Build the lexer
lexer = lex.lex()
