---
title: Projet de Compilation
subtitle: L3 Double Licence
author: Nguyen Chung Anh (12104826)
date: "26 Mai 2024"
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{lastpage}
    \pagestyle{fancy}
    \fancyfoot[CO,CE]{}
    \fancyfoot[LO,RE]{Nguyen Chung Anh}
    \fancyfoot[LE,RO]{\thepage/\pageref{LastPage}}
geometry: "right=2cm,left=2cm,top=4cm,bottom=4cm"
output: pdf_document
...

# Présentation

## Projet

Un compilateur de JavaScript vers le
langage assembleur de la mini-JS Machine. Il est sur https://gitlab.sorbonne-paris-nord.fr/12104826/compiltation-project/-/tree/main.
Il est réalisé en Python avec la librairie PLY


## Membres du groupe
- Nguyen Chung Anh

## Branches

- `parser_work` : branche de travail sur le parseur
- `master` : contient la génération de code
- `main` : contient le compilateur final


# Fragments implémentés

j'ai implémenté Fragement de 0.0 à 5.3

# Note sur les librairies utilisées

## PLY

Il est très utile mais il a des options quee je n'arrive pas à comprendre


# Conclusion

Il est une très bonne expérience pour moi pendanr ce projet. J'ai appris beaucoup de choses sur le travail avec le bibothèque et avec le git 

