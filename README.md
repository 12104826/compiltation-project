# Python JavaScript Compilator

## What is this ?
This is a project to write a JavaScript compiler as part of the end of the year's project for the Compilation course of the 3rd year of Bachelor in Computer Science at the USPN (Paris-XIII).

### How to use it ?
Use PLY, copy the ply directory into your project and import lex and yacc from the associated ply subpackage. Alternatively, you can install these files into your working python using make install.

from .ply import lex
from .ply import yacc
PLY has no third-party dependencies and can be freely renamed or moved around within your project as you see fit. It rarely changes.

### Running the Project
Simply need to run the file python main.py

